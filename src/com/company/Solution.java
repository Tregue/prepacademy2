package com.company;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Solution {
    public Contest[] contests;

    public Solution(Contest[] contests) {
        this.contests = Arrays.copyOf(contests, contests.length);
    }

    public int getScore() {
        return Arrays.stream(contests).filter(contest -> contest.played).mapToInt(contest->contest.prize).sum();
    }

    public Solution glueBackFirstContest(Contest contest) {
        Contest[] result = new Contest[contests.length+1];
        result[0] = contest;
        System.arraycopy(contests, 0, result, 1, contests.length);
        return new Solution(result);
    }

    public String toString() {
        return Arrays.stream(contests).map(Contest::toString).collect(Collectors.joining(", "))+ " -- Total :"+getScore();
    }
}
