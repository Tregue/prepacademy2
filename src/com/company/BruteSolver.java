package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BruteSolver {
    public int workDays;

    public BruteSolver(int workDays) {
        this.workDays = workDays;
    }

    public Solution bruteForce(int[] contestsValues) {
        Contest[] contests = Arrays.stream(contestsValues).mapToObj(value -> new Contest(value, true)).toArray(Contest[]::new);

        List<Solution> solutions = getSolutionsRec(contests, workDays);

        Main.writeLine("solutions: ");
        for(Solution solution:solutions) {
            Main.writeLine(solution.toString());
        }

        return solutions.stream().max(Comparator.comparing(Solution::getScore)).orElseThrow();
    }

    public List<Solution> getSolutionsRec(Contest[] contestsSubset, int remainingWorkDays) {
        List<Solution> solutions = new ArrayList<>();
        if (contestsSubset.length <= workDays) {
            if (remainingWorkDays == workDays) {
                solutions.add(new Solution(contestsSubset));
            } else {
                for (int i = 0; i < Math.min(contestsSubset.length, remainingWorkDays+1); i++) {
                    Contest[] stepContests = Contest.cloneArray(contestsSubset);
                    stepContests[i].played = false;
                    solutions.add(new Solution(stepContests));
                }
            }
        }
        else {
            Contest first = contestsSubset[0];
            Contest[] contestsMinusFirst = popFirst(contestsSubset);

            Contest rested = new Contest(first.prize, false);
            List<Solution> stepSolutions1 = getSolutionsRec(contestsMinusFirst, workDays)
                    .stream().map(solution -> solution.glueBackFirstContest(rested)).collect(Collectors.toList());
            solutions.addAll(stepSolutions1);

            if (remainingWorkDays > 0) {
                Contest played = new Contest(first.prize, true);
                List<Solution> stepSolutions2 = getSolutionsRec(contestsMinusFirst, remainingWorkDays-1)
                        .stream().map(solution -> solution.glueBackFirstContest(played)).collect(Collectors.toList());
                solutions.addAll(stepSolutions2);
            }
        }
        return solutions;
    }

    private Contest[] popFirst(Contest[] contests) {
        return Arrays.copyOfRange(contests, 1, contests.length, Contest[].class);
    }
}
