package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        writeLine("Please enter the list of contests separated by a comma (ex : 10,15,21,23) : ");
        String contestsInput = readLine();
        int[] contests;
        try {
            contests = fromUserInput(contestsInput);
        } catch (Exception e) {
            writeLine("invalid input");
            return;
        }

        writeLine("Please enter the maximum number of successive contests");
        int workDays;
        String workDaysInput = readLine();
        try {
            workDays = Integer.parseInt(workDaysInput);
        } catch (NumberFormatException e) {
            writeLine("%s is not a valid number", workDaysInput);
            return;
        }
        BruteSolver solver = new BruteSolver(workDays);
        Solution bruteSolution = solver.bruteForce(contests);
        writeLine("solution : %s", bruteSolution);
    }



    public static int[] fromUserInput(String input) {
        return Arrays.stream(input.replaceAll("\\s", "").split(",")).mapToInt(Integer::parseInt).toArray();
    }

    public static void writeLine(String s, Object... args) {
        System.out.println(String.format(s, args));
    }

    public static String readLine() {
        return scanner.nextLine();
    }
}
