package com.company;

import java.util.Arrays;

public class Contest {
    public int prize;
    public boolean played;

    public Contest(int prize, boolean played) {
        this.prize = prize;
        this.played = played;
    }

    public String toString() {
        return prize + ":" + (played?"yes":"no");
    }

    public Contest clone() {
        return new Contest(prize, played);
    }

    public static Contest[] cloneArray(Contest[] contests){
        return Arrays.stream(contests).map(Contest::clone).toArray(Contest[]::new);
    }
}
